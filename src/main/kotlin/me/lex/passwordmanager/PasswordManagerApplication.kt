package me.lex.passwordmanager

import me.lex.passwordmanager.service.PasswordManager
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PasswordManagerApplication

fun main(args: Array<String>) {
    runApplication<PasswordManagerApplication>(*args)

    /*val loginManager: LoginManager = LoginManager()

    println("Type L to login")
    println("Type C to create an account")
    val option = readlnOrNull()

    if (option != null) {
        if (option.uppercase() == "L") {
            println("Login")
            print("Enter Username: ")
            val inputUsername = readlnOrNull()
            print("Enter Password: ")
            val inputPassword = readlnOrNull()

            if (!loginManager.validateCredentials(Account(inputUsername.toString(), inputPassword.toString()))) {
                println("Credentials Invalid!")
                main(args)
            } else {
                println("Logged in!")
            }

        } else if (option.uppercase() == "C") {
            println("Create Account")
            print("Enter Username: ")
            val inputUsername = readlnOrNull()?.lowercase()
            print("Enter Password: ")
            val inputPassword = readlnOrNull()

            if (!loginManager.createAccount(Account(inputUsername.toString(), inputPassword.toString()))) {
                println("Username Taken!")
                main(args)
            } else {
                println("Account Created!")
            }
        }
    }*/
    /*val passwordManager: PasswordManager = PasswordManager()
    passwordManager.saveDetails("lex", "discord", "abc", "321")*/
}