package me.lex.passwordmanager.controller

import me.lex.passwordmanager.service.PasswordManager
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin(origins = ["http://localhost:5173"])
@RestController
@RequestMapping("/api/credentials")
class CredentialsController {
    private val passwordManager: PasswordManager = PasswordManager()

    /*@PostMapping("/save-details")
    fun saveDetails(@RequestBody request: SaveDetailsRequest) {
        passwordManager.saveDetails()
    }*/

    /*@PostMapping("/generate")
    fun generate(@RequestBody account: Account): ResponseEntity<String> {
        // Authenticate
        if (passwordManager.generate(account)) {
            return ResponseEntity.ok("Login Successful!")
        }
        return ResponseEntity.ok("Invalid Credentials!")
    }*/

    @GetMapping("/retrieve")
    fun retrieve(): String {
        return passwordManager.retrieveDetails("lex")
    }
}