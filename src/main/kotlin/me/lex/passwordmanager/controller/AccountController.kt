package me.lex.passwordmanager.controller

import me.lex.passwordmanager.model.Account
import me.lex.passwordmanager.service.LoginManager
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin(origins = ["http://localhost:5173"])
@RestController
@RequestMapping("/api")
class AccountController {
    private val loginManager: LoginManager = LoginManager()

    @PostMapping("/login")
    fun login(@RequestBody account: Account): ResponseEntity<String> {
        // Authenticate
        if(loginManager.validateCredentials(account)) {
            return ResponseEntity.ok("Login Successful!")
        }
        return ResponseEntity.ok("Invalid Credentials!")
    }

    @PostMapping("/register")
    fun createAccount(@RequestBody account: Account): ResponseEntity<String> {
        // Create User
        if(loginManager.createAccount(account)) {
            return ResponseEntity.ok("Account Created!")
        }
        return ResponseEntity.ok("Username Taken!")
    }
}