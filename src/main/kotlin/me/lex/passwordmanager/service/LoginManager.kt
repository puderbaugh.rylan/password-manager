package me.lex.passwordmanager.service

import me.lex.passwordmanager.model.Account
import java.io.FileInputStream
import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.*

class LoginManager {
    private val accountFileNameSuffix = "account.cred"

    fun createAccount(account: Account): Boolean {
        val filePath: Path = Path.of("accounts/" + account.username + "/" + accountFileNameSuffix)
        val accountCreated: Boolean

        if (filePath.toFile().exists()) {
            accountCreated = false;
        } else {
            Files.createDirectories(getDirectoryPath(account.username))
            //TODO Encrypt Password
            Files.writeString(
                filePath,
                account.username + "," + account.password,
                StandardCharsets.UTF_8,
                StandardOpenOption.CREATE
            )

            accountCreated = true
        }

        return accountCreated
    }

    fun validateCredentials(account: Account): Boolean {
        var validCredentials: Boolean = false

        val filePath: Path = getFilePath(account.username)

        if (filePath.toFile().exists()) {
            val inputStream: InputStream = FileInputStream(filePath.toString())

            val scanner = Scanner(inputStream, Charsets.UTF_8)

            while (scanner.hasNextLine()) {
                val line = scanner.nextLine()

                val fileCredentials: List<String> = line.split(",")
                val filePassword: String = fileCredentials[1]

                validCredentials = account.password == filePassword
            }
        }
        return validCredentials
    }

    private fun getFilePath(userName: String): Path {
        return Paths.get("", "accounts", userName, accountFileNameSuffix)
    }

    private fun getDirectoryPath(accountName: String): Path {
        return Paths.get("", "accounts/", accountName)
    }
}
