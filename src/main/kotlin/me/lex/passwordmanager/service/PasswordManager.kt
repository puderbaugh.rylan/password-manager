package me.lex.passwordmanager.service

import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.security.SecureRandom
import java.util.*

class PasswordManager {
    companion object {
        private const val DETAILS_FILE_NAME_SUFFIX = "login-details.cred"
        private const val MIN_PASSWD_LENGTH = 8
    }

    //    private val encryption: Encryption = Encryption()
    private var encryptedInformation: String? = null

    fun generatePassword(length: Int): String {
        val lowercaseLetters = "abcdefghijklmnopqrstuvwxyz".toCharArray()
        val uppercaseLetters = "ABCDEFGJKLMNPRSTUVWXYZ".toCharArray()
        val numbers = "0123456789".toCharArray()
        val symbols = "^$?!@#%&".toCharArray()
        val allAllowed = "abcdefghijklmnopqrstuvwxyzABCDEFGJKLMNPRSTUVWXYZ0123456789^$?!@#%&".toCharArray()

        // Use cryptographically secure random number generator
        val random: Random = SecureRandom()
        val password = StringBuilder()

        for (i in 0 until length - 4) {
            password.append(allAllowed[random.nextInt(allAllowed.size)])
        }

        // Ensure password policy is met by inserting required random chars in random positions
        password.insert(random.nextInt(password.length), lowercaseLetters[random.nextInt(lowercaseLetters.size)])
        password.insert(random.nextInt(password.length), uppercaseLetters[random.nextInt(uppercaseLetters.size)])
        password.insert(random.nextInt(password.length), numbers[random.nextInt(numbers.size)])
        password.insert(random.nextInt(password.length), symbols[random.nextInt(symbols.size)])
        return password.toString()
    }

    fun saveDetails(accountName: String, name: String, username: String, password: String) {
        // Encrypt Information
        /*val encryptedName = encryption.encrypt(name)
        val encryptedUsername = encryption.encrypt(username)
        val encryptedPassword = encryption.encrypt(password)*/

        // Write to file
        val fileContent = "$name,$username,$password:"

        //TODO Fix Path (Hardcoded Right Now)
        if (Files.exists(Path.of("accounts/$accountName/login-details.cred"))) {
            if (!checkDuplicateDetails(accountName, fileContent)) {
                try {
                    Files.write(
                        //TODO Fix Path (Hardcoded Right Now)
                        Path.of("accounts/$accountName/login-details.cred"),
                        fileContent.toByteArray(),
                        StandardOpenOption.APPEND
                    )
                    println("Details Saved!")
                } catch (ex: IOException) {
                    throw RuntimeException(ex)
                }
            } else {
                println("These details have already been stored!")
            }
        } else {
            try {
                //TODO Fix Path (Hardcoded Right Now)
                Files.writeString(Path.of("accounts/$accountName/login-details.cred"), fileContent)
            } catch (ex: IOException) {
                throw RuntimeException(ex)
            }
        }
    }

    fun retrieveDetails(accountName: String): String {
        val loginDetails = StringBuilder()
        var decryptedDetails: Array<String?>
        try {
            //TODO Fix Path (Hardcoded Right Now)
            val encryptedInformation = Files.readString(Path.of("accounts/$accountName/login-details.cred"))
            val entry = encryptedInformation.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (entries in entry) {
                decryptedDetails = getLoginDetails(entries)
                loginDetails.append("Name: ")
                loginDetails.append(decryptedDetails[0])
                loginDetails.append(", Username: ")
                loginDetails.append(decryptedDetails[1])
                loginDetails.append(", Password: ")
                loginDetails.append(decryptedDetails[2])
                loginDetails.append("\n")
            }
        } catch (e: IOException) {
            println("Error: No details have been saved!")
            throw  RuntimeException(e);

        }
        return loginDetails.toString()
    }

    private fun getLoginDetails(entry: String): Array<String?> {
        val loginDetails: Array<String?> = entry.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        /*loginDetails[0] = encryption.decrypt(loginDetails[0])
        loginDetails[1] = encryption.decrypt(loginDetails[1])
        loginDetails[2] = encryption.decrypt(loginDetails[2])
        loginDetails[3] = encryption.decrypt(loginDetails[3])*/
        return loginDetails
    }

    private fun checkDuplicateDetails(accountName: String, content: String): Boolean {
        var hasMatch = false
        encryptedInformation = try {
            //TODO Fix Path (Hardcoded Right Now)
            Files.readString(Path.of("accounts/$accountName/login-details.cred"))
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
        val fileDetails: Scanner
        try {
            //TODO Fix Path (Hardcoded Right Now)
            fileDetails = Scanner(Path.of("accounts/$accountName/login-details.cred"))
        } catch (e: IOException) {
            throw RuntimeException(e)
        }

        // Checks if there is a duplicate entry
        val fileUserDetails: String = fileDetails.nextLine()
        val userDetails = fileUserDetails.split("(?<=:)".toRegex()).dropLastWhile { it.isEmpty() }
            .toTypedArray()

        //TODO convert to while loop (break early)
        for (details in userDetails) {
            if (details == content) {
                hasMatch = true
                break
            }
        }
        return hasMatch
    }

    private fun validatePasswdLength(passwdLength: Int): Boolean {
        val isValid: Boolean

        // Check if passwd length is a valid length
        if (passwdLength >= MIN_PASSWD_LENGTH) {
            isValid = true
        } else {
            isValid = false
            println("Password length is too short! The minimum length is $MIN_PASSWD_LENGTH")
        }
        return isValid
    }

    fun exec(passwdLength: Int): String? {
        var passwd: String? = null
        if (validatePasswdLength(passwdLength)) {
            passwd = generatePassword(passwdLength)
        }
        return passwd
    }

    private fun getFilePath(accountName: String): Path {
        return Paths.get("", DETAILS_FILE_NAME_SUFFIX)
    }
}