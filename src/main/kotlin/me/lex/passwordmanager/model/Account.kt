package me.lex.passwordmanager.model

data class Account(
    val username: String,
    val password: String
)